import {css} from "@/utils/dom";

export function getElementProps (el, offset) {
    let { top, left, right, bottom, width, height } = el.getBoundingClientRect()

    // 0 index represent space from the element on X-axis
    // 1 index represent distance from the element on Y-axis
    if (offset !== void 0) {
        top -= offset[1]
        left -= offset[0]
        bottom += offset[1]
        right += offset[0]
        width += offset[0]
        height += offset[1]
    }
    return {
        top,
        left,
        right,
        bottom,
        width,
        height,
        middle: left + (right - left) / 2,
        center: top + (bottom - top) / 2
    }
}
export function getTargetOffset (el) {
    return {
        top: 0,
        center: el.offsetHeight / 2,
        bottom: el.offsetHeight,
        left: 0,
        middle: el.offsetWidth / 2,
        right: el.offsetWidth
    }
}
export function getScrollbarWidth () {
    const
        inner = document.createElement('p'),
        outer = document.createElement('div')

    css(inner, {
        width: '100%',
        height: '200px'
    })
    css(outer, {
        position: 'absolute',
        top: '0px',
        left: '0px',
        visibility: 'hidden',
        width: '200px',
        height: '150px',
        overflow: 'hidden'
    })

    outer.appendChild(inner)

    document.body.appendChild(outer)

    const w1 = inner.offsetWidth
    outer.style.overflow = 'scroll'
    let w2 = inner.offsetWidth

    if (w1 === w2) {
        w2 = outer.clientWidth
    }

    // outer.remove()
    return  w1 - w2;
}
