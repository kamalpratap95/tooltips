import Vue from 'vue'
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes= [
    {
        path:'/',
        component: ()=> import('../layout/landing/LandingLayout'),
        children:[
            {
                path:'',
                component: () => import('../pages/landing/LandingPage')
            },
            {
                path: '/tool-tip',
                name:'ToolTip',
                component: () => import('../pages/tooltip/ToolTipPlayground'),
            }
        ]
    }]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router;
